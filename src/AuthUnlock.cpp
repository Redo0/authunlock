
#include <Windows.h>
#include <Psapi.h>

#include "RedoBlHooks.hpp"

bool init(){
	BlInit;
	
	ADDR BlScanHex(addrLock, "C6 05 ? ? ? ? ? C6 05 ? ? ? ? ? C3");
	
	BlPatchBytes(7, addrLock, "\x90\x90\x90\x90\x90\x90\x90");
	BlPatchByte(addrLock+13, 1);
	
	return true;
}

bool deinit(){
	return true;
}

int __stdcall DllMain( HINSTANCE hInstance,  unsigned long reason,  void *reserved ){
	switch(reason){
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}
