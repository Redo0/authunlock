
AuthUnlock by Redo
Lets you join internet games in offline mode.

How to install:
1. Install RedBlocklandLoader - gitlab.com/Redo0/RedBlocklandLoader
2. Install AuthUnlock.dll into the modules folder
3. Disable connecting to the auth server by blocking the auth server in your hosts file
	a. Run Notepad as Administrator
	b. Open `C:\Windows\System32\drivers\etc\hosts`
	c. Add a line that says `0.0.0.0 auth.blockland.us`
	d. Save the file and restart Blockland
	e. To revert this change, repeat the process but remove the line from hosts

How to use:
1. Start Blockland.
	If the hosts file step was done correctly, it should fail to connect to the auth server, and end up in offline mode.
2. Enter a valid BL key to get out of Demo mode, if necessary
3. Enter `lock();` in the console to unlock
4. Join servers using `ConnectToServer("ip:port", "password", 1, 0);`
	You will only be able to connect to servers that have auth disabled.
